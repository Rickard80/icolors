package com.snowscape.icolors.data

import android.graphics.Color

data class RgbColor(val color: Int) {

    var red     : Int = 255
    var green   : Int = 255
    var blue    : Int = 255

    init {
        red = Color.red(color)
        green = Color.green(color)
        blue = Color.blue(color)
    }

    fun convertToString() : String {
        return "$red, $green, $blue"
    }

}