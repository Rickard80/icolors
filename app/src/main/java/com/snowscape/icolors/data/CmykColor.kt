package com.snowscape.icolors.data

import android.graphics.Color

class CmykColor(color: Int) {

    var cyan    = 1.0
    var magenda = 1.0
    var yellow  = 1.0
    var black   = 0.0

    init {
        val red   : Double = Color.red(color).toDouble() / 255.0
        val green : Double = Color.green(color).toDouble() / 255.0
        val blue  : Double = Color.blue(color).toDouble() / 255.0

        black = 1.00 - Math.max( Math.max(red, green), blue)

        if (black < 1.0) {
            cyan = 1 - red - black
            magenda = (1 - green - black) / (1 - black)
            yellow = (1 - blue - black) / (1 - black)
        }
    }

    fun convertToString() : String {
        return cutDecimals(cyan, 2) + ", " +
               cutDecimals(magenda, 2) + ", " +
               cutDecimals(yellow, 2) + ", " +
               cutDecimals(black, 2)
    }

    fun cutDecimals(cmykColor: Double, noOfFractions: Int) : String {
        return String.format("%.$noOfFractions"+"f", cmykColor)
    }
}