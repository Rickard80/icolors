package com.snowscape.icolors.data

class HexColor(color: Int) {
    var hexColor : String = "FFFFFF"

    init {
        hexColor = String.format("%06X", color)
    }

    fun convertToString() : String {
        return "#" + hexColor.substring(2)
    }
}