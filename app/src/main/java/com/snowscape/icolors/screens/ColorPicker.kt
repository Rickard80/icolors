package com.snowscape.icolors.screens

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.madrapps.pikolo.listeners.SimpleColorSelectionListener
import com.snowscape.icolors.R
import com.snowscape.icolors.data.CmykColor
import com.snowscape.icolors.data.HexColor
import com.snowscape.icolors.data.RgbColor
import com.snowscape.icolors.utils.MySharedPreferences
import kotlinx.android.synthetic.main.color_picker_activity.*

class ColorPicker : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.color_picker_activity)

        val context = this

        val startingColor = MySharedPreferences(context).readColor("startingColor")

        colorpicker.setColor(startingColor)
        updateEverything(startingColor)

        colorpicker.setColorSelectionListener(object: SimpleColorSelectionListener() {
            override fun onColorSelected(color: Int) {
                super.onColorSelected(color)
println("color: $color")
                updateEverything(color)
            }

            override fun onColorSelectionEnd(color: Int) {
                super.onColorSelectionEnd(color)

                MySharedPreferences(context).saveColor("startingColor", color)
            }
        })
    }

    private fun updateEverything(color: Int) {
        (root as View).setBackgroundColor(color)
        irisImageView.setColorFilter(color)
        landscapeBackgroundView?.setBackgroundColor(color)

        hexaText.setText( HexColor(color).convertToString()  )
        rgbaText.setText( RgbColor(color).convertToString()  )
        cmykText.setText( CmykColor(color).convertToString() )
    }

}
