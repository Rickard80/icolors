package com.snowscape.icolors.utils

import android.content.Context

class MySharedPreferences(context: Context) {
    private val storageKey = "myPref"
    private val context : Context?

    init {
        this.context = context
    }

    fun saveColor(colorKey: String, color: Int) {
        val sharedPref = context!!.getSharedPreferences(storageKey, Context.MODE_PRIVATE)

        val editor = sharedPref.edit()

        editor.putInt(colorKey, color)
        editor.apply()
    }

    fun readColor(colorKey: String) : Int {
        val goldenAsDefault = -4546208
        val sharedPref = context!!.getSharedPreferences(storageKey, Context.MODE_PRIVATE)

        return sharedPref.getInt(colorKey, goldenAsDefault)
    }
}